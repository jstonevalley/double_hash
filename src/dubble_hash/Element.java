package dubble_hash;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.Collections;
import java.util.Random;

public class Element {
	private ArrayList<BitSet> original;
	private BitSet[] hashMatrix;
	private BitSet[] smallTable;

	public Element() {
		original = new ArrayList<BitSet>();
	}

	public Element(BitSet value) {
		original = new ArrayList<BitSet>();
		original.add(value);
	}

	public void add(BitSet value) {
		original.add(value);
	}

	public int count() {
		return original.size();
	}

	public int countHash() {
		if (smallTable == null) {
			return 0;
		}
		int count = 0;
		for (int i = 0; i < smallTable.length; i++) {
			if (smallTable[i] != null) {
				count++;
			}
		}
		return count;
	}

	public void hashOriginal() {
		Random random = new Random();
		ArrayList<BitSet> input = new ArrayList<BitSet>(original.size());
		for (BitSet bitSet : original) {
			input.add(new BitSet());
		}
		Collections.copy(input, original);
		while (countHash() != count()) {
			smallTable = new BitSet[(int) Math.pow(2,
					(int) Math.ceil(Math.log(original.size()) / Math.log(2)))];
			hashMatrix = new BitSet[(int) Math.ceil(Math.log(original.size()) / Math.log(2))];
			for (int i = 0; i < hashMatrix.length; i++) {
				hashMatrix[i] = Hashing.convert(random.nextLong());
			}
			hashInput(input);
		}
	}

	private void hashInput(ArrayList<BitSet> input) {
		for (BitSet bitSet : input) {
			BitSet indexBitSet = hash(bitSet);
			int index = (int) Hashing.convert(indexBitSet);
			smallTable[index] = bitSet;
		}
	}

	private BitSet hash(BitSet value) {
		BitSet result = new BitSet();
		BitSet argument = new BitSet();
		for (int i = 0; i < hashMatrix.length; i++) {
			argument = (BitSet) value.clone();
			argument.and(hashMatrix[i]);
			int cardinality = argument.cardinality();
			if (cardinality % 2 == 1) {
				result.set(i, true);
			}
		}
		return result;
	}

	@Override
	public String toString() {
		String string = hashMatrix.length + "\n";
		for (int i = 0; i < hashMatrix.length; i++) {
			String hexString = Long.toHexString(Hashing.convert(hashMatrix[i]));
			while (hexString.length() < 16) {
				hexString = "0" + hexString;
			}
			string = string + hexString.toUpperCase() + "\n";
		}
		return string + "\n";
	}

}
