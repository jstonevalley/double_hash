package dubble_hash;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.BitSet;

public class Hashing {
	private Kattio io;
	private BitSet[] hashMatrix;
	private Element[] elements = new Element[4194304];

	public Hashing(Kattio io, Kattio ioMatrix) throws IOException {
		this.io = io;
		hashMatrix = new BitSet[22];
		for (int i = 0; i < hashMatrix.length; i++) {
			hashMatrix[i] = convert(Long.parseLong(ioMatrix.getWord(), 16));
		}
		hashInput();
		int clashes = 0;
		for (int i = 0; i < elements.length; i++) {
			if (elements[i] != null && elements[i].count() > 2) {
				clashes = clashes + elements[i].count();
			}
		}
		System.err.println(clashes);
		for (int i = 0; i < elements.length; i++) {
			if (elements[i] != null && elements[i].count() > 2) {
				elements[i].hashOriginal();
			}
		}
		clashes = 0;
		for (int i = 0; i < elements.length; i++) {
			if (elements[i] != null && elements[i].countHash() > 2) {
				clashes = clashes + 1;
			}
		}
		System.err.println(clashes);
		BufferedWriter bw = new BufferedWriter(new FileWriter(new File("output.txt")));
		for (int i = 0; i < hashMatrix.length; i++) {
			String hexString = Long.toHexString(convert(hashMatrix[i]));
			while (hexString.length() < 16) {
				hexString = "0" + hexString;
			}
			bw.write(hexString.toUpperCase() + "\n");
		}
		bw.write("\nNumber of Hi created where 3 or more initial values are hashed to the same index: "
				+ clashes + "\n\n");
		bw.flush();
		for (int i = 0; i < elements.length; i++) {
			if (elements[i] != null && elements[i].countHash() > 2) {
				bw.write(i + "\n" + elements[i].toString());
			}
		}
		bw.flush();
		bw.close();
		System.err.println("Klarre");
	}

	private void hashInput() {
		String word = io.getWord();
		while (word != null) {
			Long value = Long.parseLong(word, 16);
			BitSet bitSet = convert(value);
			BitSet indexBitSet = hash(bitSet);
			int index = (int) convert(indexBitSet);
			if (elements[index] == null) {
				elements[index] = new Element(bitSet);
			} else {
				elements[index].add(bitSet);
			}
			word = io.getWord();
		}
	}

	private BitSet hash(BitSet value) {
		BitSet result = new BitSet();
		BitSet argument = new BitSet();
		for (int i = 0; i < hashMatrix.length; i++) {
			argument = (BitSet) value.clone();
			argument.and(hashMatrix[i]);
			int cardinality = argument.cardinality();
			if (cardinality % 2 == 1) {
				result.set(i, true);
			}
		}
		return result;
	}

	public static BitSet convert(long value) {
		BitSet bits = new BitSet();
		int index = 0;
		while (value != 0L) {
			if (value % 2L != 0) {
				bits.set(index);
			}
			++index;
			value = value >>> 1;
		}
		return bits;
	}

	public static long convert(BitSet bits) {
		long value = 0L;
		for (int i = 0; i < bits.length(); ++i) {
			value += bits.get(i) ? (1L << i) : 0L;
		}
		return value;
	}
}
